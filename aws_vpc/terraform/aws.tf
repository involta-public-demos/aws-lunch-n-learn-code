provider "aws" {
  profile = var.profile[terraform.workspace]
  assume_role {
      role_arn = "arn:aws:iam::${var.account_id[terraform.workspace]}:role/${var.role_name[terraform.workspace]}"
  }
  region  = var.region[terraform.workspace]
}