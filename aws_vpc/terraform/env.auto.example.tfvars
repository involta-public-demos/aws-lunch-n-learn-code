azs = {
    lunch-n-learn = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

cidr = {
    lunch-n-learn = "10.0.0.0/16"
}

name = {
    lunch-n-learn = "Lunch and Learn Demo VPC"
}


private_subnets = {
    lunch-n-learn = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
}

private_usage = {
    lunch-n-learn = "private"
}

public_usage = {
    lunch-n-learn = "public"
}

public_subnets = {
    lunch-n-learn = ["10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24"]
}

tags = {
    lunch-n-learn = {
            costcenter = "12345",
            environment = "demo",
            application = "hungry"
        }
}

profile = {
    lunch-n-learn = "[redacted]"
}

account_id = {
    lunch-n-learn = "[redacted]"
}

role_name = {
    lunch-n-learn = "[redacted]"
}

region = {
    lunch-n-learn = "us-east-1"
}
