# VPC
resource "aws_vpc" "main" {

  cidr_block           = var.cidr[terraform.workspace]
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = merge(
    {
      "Name" = format("%s", var.name[terraform.workspace])
    },
    var.tags[terraform.workspace]
  )
}

# Internet Gateway
resource "aws_internet_gateway" "main" {

  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      "Name" = format("%s", var.name[terraform.workspace])
    },
    var.tags[terraform.workspace]
  )
}

# Publiс routes
resource "aws_route_table" "public" {

  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      "Name" = format("%s-public", var.name[terraform.workspace])
    },
    var.tags[terraform.workspace]
  )
}

resource "aws_route" "public_internet_gateway" {

  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id

  timeouts {
    create = "5m"
  }
}


resource "aws_route_table" "private" {
  count = length(var.private_subnets[terraform.workspace])

  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      "Name" = format("%s-private", var.name[terraform.workspace])
    },
    var.tags[terraform.workspace]
  )
}


# Public subnet
resource "aws_subnet" "public" {
  count = length(var.public_subnets[terraform.workspace])

  vpc_id                  = aws_vpc.main.id
  cidr_block              = element(concat(var.public_subnets[terraform.workspace], [""]), count.index)
  availability_zone       = element(var.azs[terraform.workspace], count.index)
  map_public_ip_on_launch = false

  tags = merge(
    {
      "Name" = format(
        "%s-${var.public_usage[terraform.workspace]}-%s",
        var.name[terraform.workspace],
        element(var.azs[terraform.workspace], count.index),
      )
    },
    {
      "Usage" = var.public_usage[terraform.workspace]
    },
    var.tags[terraform.workspace]
  )
}   

# Private subnet
resource "aws_subnet" "private" {
  count = length(var.private_subnets[terraform.workspace])

  vpc_id            = aws_vpc.main.id
  cidr_block        = var.private_subnets[terraform.workspace][count.index]
  availability_zone = element(var.azs[terraform.workspace], count.index)

  tags = merge(
    {
      "Name" = format(
        "%s-${var.private_usage[terraform.workspace]}-%s",
        var.name[terraform.workspace],
        element(var.azs[terraform.workspace], count.index),
      )
    },
    {
      "Usage" = var.private_usage[terraform.workspace]
    },
    var.tags[terraform.workspace]
  )
}
# NAT Gateway
resource "aws_eip" "nat" {
  count = length(var.public_subnets[terraform.workspace])

  vpc = true

  tags = merge(
    {
      "Name" = format("%s-nat-gateway", var.name[terraform.workspace])
    },
    var.tags[terraform.workspace]
  )
}


##########################
# Route table association
##########################
resource "aws_route_table_association" "private" {
  count = length(var.private_subnets[terraform.workspace])

  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

resource "aws_route_table_association" "public" {
  count          = length(var.public_subnets[terraform.workspace])
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}
