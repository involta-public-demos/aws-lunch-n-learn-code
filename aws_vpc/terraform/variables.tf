variable "name" {
  description = "name for vpc"
  type        = map(string)
}

variable "cidr" {
  description = "Primary CIDR block for the VPC"
  type        = map(string)
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(map(string))
  default     = {}
}

variable "public_usage" {
  description = "Suffix to append to public subnets name"
  type        = map(string)
}

variable "private_usage" {
  description = "Suffix to append to private subnets name"
  type        = map(string)
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  type        = map(list(string))
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  type        = map(list(string))
}

variable "azs" {
  description = "A list of availability zones in the region"
  type        = map(list(string))
}

variable "region" {
  description = "Region to create this VPC in (e.g. us-east-1)"
  type        = map(string)
}

variable "profile" {
  description = "The local AWS profile name"
  type        = map(string)
}

variable "account_id" {
  description = "Which AWS account to create the VPC in"
  type        = map(string)
}

variable "role_name" {
  description = "Name of the role to assume in the target account"
  type        = map(string)
}


