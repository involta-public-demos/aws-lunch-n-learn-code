
var desiredLoadFactor = .2;

function blockCpuFor(ms) {
	var now = new Date().getTime();
	var result = 0
	while(true) {
		result += Math.random() * Math.random();
		if (new Date().getTime() > now +ms)
			return;
	}	
}

const start = (duration_sec) => {
    for (let i = 0; i < duration_sec; i++) {
        setTimeout(() => {
            blockCpuFor(1000*desiredLoadFactor)
        },1000*i)
    }
}

exports.load = start
