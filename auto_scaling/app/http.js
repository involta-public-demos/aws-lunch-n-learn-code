const http = require('http');
const os = require('os')
const { spawn } = require('child_process');


// Create an HTTP tunneling proxy
const s = http.createServer((req, res) => {
    console.log(req.url)
    
    if (req.url == "/") {
        console.log(process.argv[0])
        const sub = spawn('nice', ['-n','-4',process.argv[0],'load_w.js'], {
            detached: true               // this removes ties to the parent
          })
    }
    res.writeHead(200, { 'Content-Type': 'text/plain' })
    res.end(os.hostname() + "\r\n" + "\r\n")
})

// Now that proxy is running
s.listen(1337, '0.0.0.0', () => {})
  