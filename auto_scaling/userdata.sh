#!/bin/bash -xe
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

pwd

cd ~

curl -o http.js https://gitlab.com/involta-public-demos/aws-lunch-n-learn-code/-/raw/master/auto_scaling/app/http.js
curl -o load.js https://gitlab.com/involta-public-demos/aws-lunch-n-learn-code/-/raw/master/auto_scaling/app/load.js
curl -o load_w.js https://gitlab.com/involta-public-demos/aws-lunch-n-learn-code/-/raw/master/auto_scaling/app/load_w.js


curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash

. /.nvm/nvm.sh
nvm install node

node http.js
